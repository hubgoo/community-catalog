#!/bin/bash

# This will do various API items very easily - eventually we will add parameters for different providors e.g. CloudFlare,Go Daddy, AMazon - but often we will need to look up intermediate
#items and the user does not necessarily neeed to knwo which providor requires a change - may even be multiple chagnes
# Methodology is to allow user to use the minimum possible specification that has a sensible conclusion!!

#Test commmands
#curl -X PUT "https://api.digitalocean.com/v2/domains/hubgoo.com/records/2353606" -d '{"name":"nd050.hubgoo.com.","data":"90.194.210.0"}' -H  "Authorization: Bearer $TOKEN" -H "Content-Type: application/json"
#curl -X PUT "https://api.digitalocean.com/v2/domains/hubgoo.com/records/2353606" -d '{"name":"nd050.hubgoo.com.","data":"176.248.147.156"}' -H  "Authorization: Bearer $TOKEN" -H "Content-Type: application/json"
#


#pointless test below!!!
#$log && log=true
$log && echo "Logging is set to $log export log=false to disable (this is needed for json processing)"

$log && echo "starting up..."


# Argument = -t test -r server -p password -v

usage()
{
cat << EOF
usage: $0 options

This script runs the A or nd050

OPTIONS:
   -h      Show this message
   -t      Test type, can be 'A' , 'nd050' or 'cloudA','cloudC' or 'rec_edit'
   -i      ip address or d to detect
   -a    Server a Name
   -d    (sub)domain
   -v      Verbose
EOF
}

TEST=
SERVER=
NAME=
VERBOSE=
bURL=
DOMAIN="hubgoo.co.uk"
while getopts �ht:i:a:u:d:v� OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         t)
             TEST=$OPTARG
             ;;
         i)
             SERVER=$OPTARG
             ;;
         a)
             NAME=$OPTARG
             ;;
	 u)
             bURL=$OPTARG
             ;;
	 d)
             DOMAIN=$OPTARG
             ;;
         v)
             VERBOSE=1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $TEST ]] || [[ -z $SERVER ]] || [[ -z $NAME ]]
then
     usage
     exit 1
fi



#cloudflare
cloudflaregateway="https://www.cloudflare.com/api_json.html"
#export CLOUDFLAREAPIKEY=a35463a68aa182d097ddcb67e7971d1fa82a5
export CLOUDFLAREAPIKEY=1cb1240df43e44111c0d64349a8e078f680cf
export email="johnbeck@definition.tv"
export tkn=$CLOUDFLAREAPIKEY
export clouddomain="hubgoo.co.uk"

#example stats
cloudcmd="curl https://www.cloudflare.com/api_json.html   -d 'a=stats'   -d \"tkn=$tkn\"   -d 'email=johnbeck@definition.tv'   -d 'z=hubgoo.co.uk'   -d 'interval=20'"

#Digitalocean - Current Token
export TOKEN=7ce2716036229c7d12be8580dd89dc142fe7b12cb49d0e6437c37d76500d5d13


export THISIP=`getip | tr -d '\n' ` >/dev/null
export THISHOST="dummy"
#echo "Detected IP is $THISIP"

[[ d != $SERVER  ]] && export THISIP=$SERVER


#export THISIP=$SERVER
export THISHOST=$NAME


$log && echo " This IP is now $THISIP  and the host is $THISHOST"


#Get all DNS
[[ $TEST == rec_load_all ]] && export rec_load_all="curl https://www.cloudflare.com/api_json.html --stderr /dev/null \
  -d 'a=rec_load_all' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain'" && $log && echo "recload all"


[[ $TEST == zone_settings ]] && export zone_settings="curl https://www.cloudflare.com/api_json.html \
  -d 'a=zone_settings' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain'"

$log && echo "zone"

#minify
[[ $TEST == cloud_minify ]] && export cloud_minify="curl https://www.cloudflare.com/api_json.html \
  -d 'a=minify' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'v=7'"

#purge a record"
[[ $TEST == cloud_purge ]] && export cloud_purge="curl https://www.cloudflare.com/api_json.html \
  -d 'a=zone_file_purge' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'url=$bURL'"

[[ $TEST == cloudA ]] && export cloudA="curl https://www.cloudflare.com/api_json.html -s  \
  -d 'a=rec_new' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'type=A' \
  -d 'name=$NAME' \
  -d 'content=$THISIP' \
  -d 'ttl=120'" && $log && echo "got cloudA"



[[ $TEST == cloudC ]] && export cloudC="curl https://www.cloudflare.com/api_json.html -s  \
  -d 'a=rec_new' \
  -d 'tkn=$tkn' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'type=CNAME' \
  -d 'name=$NAME' \
  -d 'content=$THISIP' \
  -d 'ttl=120'" && $log && echo "got cloudC"



uutest="
curl https://www.cloudflare.com/api_json.html --stderr /dev/null \
  -d 'a=rec_new' \
  -d 'tkn=1cb1240df43e44111c0d64349a8e078f680cf' \
  -d 'email=johnbeck@defintiion.tv' \
  -d 'z=hubgoo.co.uk' \
  -d 'type=A'
  -d 'name=acer' \
  -d 'content=1.2.3.4' \
  -d 'ttl=1'"


[[ $TEST == get_rec ]] && export get_rec="export log=false;defdns.sh -t rec_load_all -i $THISIP -a $DOMAIN | jq '.response? | .recs | .objs | .[] | {name,rec_id} | select(.name == \"$THISHOST\") | .rec_id'"

$log && echo "record edit - A type only"

$log && echo " trying to get record"
#echo "recid=`defdns.sh -t get_rec -i $THISIP -a $NAME -d $DOMAIN`"

#[[ $TEST == rec_edit ]] && export log=true
#[[ $TEST == rec_edit ]] && $log && echo "got a rec of $recid test is $TEST"

#[[ $TEST == rec_edit ]] && $log && echo " tested ok "
export log=false
[[ $TEST == rec_edit ]] && recid=$(defdns.sh -t get_rec -i $THISIP -a $NAME -d $DOMAIN)
#[[ $TEST == rec_edit ]] && echo "initial set is $(expr match $recid	'.\([0-9]*\)') and it has a terminus"
[[ $TEST == rec_edit ]] && recid=$(expr match $recid	'.\([0-9]*\)')
#[[ $TEST == rec_edit ]] && recid=`cat $recid | tr -d '\n'` >/dev/null


#[[ $TEST == rec_edit ]] && echo "got a changed rcord id of $recid ..ok"

#[[ $TEST == rec_edit ]] && export log=true

export rec_edit="curl https://www.cloudflare.com/api_json.html -s  \
  -d 'a=rec_edit' \
  -d 'tkn=$tkn' \
  -d 'id=$recid' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'type=A' \
  -d 'name=$NAME' \
  -d 'content=$THISIP' \
  -d 'ttl=1'"
# -d 'service_mode=0' \

#[[ $TEST == rec_edit ]] && echo "record edit set to something"

[[ $TEST == manual_edit ]] && export manual_edit="recid=\(defdns.sh -t get_rec -i $THISIP -a $NAME -d $DOMAIN\) && curl https://www.cloudflare.com/api_json.html -s  \
  -d 'a=rec_edit' \
  -d 'tkn=$tkn' \
  -d 'id=173662762' \
  -d 'email=$email' \
  -d 'z=$clouddomain' \
  -d 'type=A' \
  -d 'name=$NAME' \
  -d 'content=$THISIP' \
  -d 'service_mode=1' \
  -d 'ttl=1'"


$log && echo "manual _edit  set "

#set nd050 to ip given or detected
[[ $TEST == nd050 ]] && export nd050="curl -X PUT \"https://api.digitalocean.com/v2/domains/hubgoo.com/records/2353606\" -d '{\"name\":\"nd050.hubgoo.com.\",\"data\":\"$THISIP\"}' -H \"Authorization: Bearer $TOKEN\" -H \"Content-Type: application/json\""

#set an a record to second parameter

[[ $TEST == digitalA ]] && export digitalA="curl -H \"Authorization: Bearer $TOKEN\" -H \"Content-Type: application/json\" -d '{\"type\": \"A\", \"name\": \"$THISHOST\", \"data\": \"$THISIP\"}' -X POST \"https://api.digitalocean.com/v2/domains/hubgoo.com/records\""


#[[ $TEST=A]] && echo $cloud_getdns && bash -c "$cloud_getdns"

#$log && echo "ready to test"

[[ $TEST == rec_edit ]]  && echo "record found: $recid"

$log && echo "comamand is : ${!TEST}"
#[[ $TEST == rec_edit ]] && exit;


#&& bash -c "${!TEST} 2>&1"

bash -c "${!TEST}"  | tr -d '\n'
