#docker run --rm -v <SOURCE_DATA_VOLUME_NAME>:/from alpine ash -c "cd /from ; tar -cf - . " | ssh <TARGET_HOST> 'docker run --rm -i -v <TARGET_DATA_VOLUME_NAME>:/to alpine ash -c "cd /to ; tar -xpvf - " '
docker run --rm -v $1:/from alpine ash -c "cd /from ; tar -cf - . " | ssh -A $2 'docker run --rm -i -v $3:/to alpine ash -c "cd /to ; tar -xpvf - " '
