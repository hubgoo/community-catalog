// RethinkDB settings
exports.host = 'db';    // RethinkDB host
exports.port = 28015;          // RethinkDB driver port
exports.authKey = undefined;   // RethinkDB authentification key (optional)
exports.user = undefined;       // RethinkDB user name (optional)
//exports.password = 'jason'   // RethinkDB user password (optional)
exports.ssl = undefined;        // RethinkDB SSL certificate config (optional)

// Express settings
exports.expressPort = 3032;    // Port used by express
exports.debug = true;          // Debug mode
//exports.network = '127.0.0.1'  // Network the node app will run on
exports.network = '0.0.0.0'  // Network the node app will run on ... change for security

