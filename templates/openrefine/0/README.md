# Open Refine 



**This Template creates 2 services:**
- **parity:** This is the scalable database service.
- **parity-proxy:** This is the proxy service, the query-router and admin panel. 

### Repository

### Variables
- **Proxy Web Port:** admin panel port (default: 8780)
